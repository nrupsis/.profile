<table>
    <tr><td><strong>blender.chat</strong></td><td><a href='https://blender.chat/direct/Nate-Rupsis'>Nate Rupsis</a>  </td></tr>
    <tr><td><strong>website</strong></td><td><a href='https://blunder.training'>Blender development notes</a>  </td></tr>
    <tr><td><strong>Mastodon    </strong></td><td><a href='https://mastodon.art/@nrupsis'>@nrupsis@mastodon.art</a></td></tr>
</table>


# About me

I'm a Blender enthusiast and community developer. I've been working with the [Animation & Rigging module](https://projects.blender.org/blender/blender/wiki/Module:%20Animation%20&%20Rigging) since 2021. 

I started my Blender journey around 2007 ([v2.43](https://commons.wikimedia.org/wiki/File:Blender_2.43-splash.jpg)). I took a brief hiatus to get my master's degree in computer science. Now I'm back trying to help build useful tools in Blender. 

If anyone is interested in contributing to Blender, please reach out to me on blender chat and I'll help you the best I can!
